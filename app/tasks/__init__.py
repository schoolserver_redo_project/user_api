from flask import Blueprint, request
from flask_jsonpify import jsonpify
from flask_cors import cross_origin
from _helpers import create_instance
from app import CORS

tasks = Blueprint('tasks', __name__, template_folder='templates')


@cross_origin
@tasks.route('/fetch/')
def get_tasks():
    """
    Returns a list of all tasks.
    """
    iserv = create_instance(request)
    iserv._login()
    tasks_objects = iserv.get_tasks()
    if request.args.get("old"):
        # TODO: Make this works when its implemented in scrapper
        return "Fetching old tasks is not yet implemented on the Scrapper side", 501
    if request.args.get("details"):
        for task_object2 in tasks_objects:
            task_object2.get_details()
    to_publish = {
        "tasks": []
    }
    for task_object in tasks_objects:
        task_dict = {
            "title": task_object.title,
            "description": task_object.description,
            "author": task_object.author,
            "start_date": task_object.start_date,
            "end_date": task_object.end_date,
            "done": task_object.done,
            "corrections": task_object.corrections,
            "provided_files": task_object.provided_files,
            "id": task_object.id,
        }

        to_publish["tasks"].append(task_dict)
    return jsonpify(to_publish)
