import re

from flask_cors import cross_origin
from flask_jsonpify import jsonpify
from flask import Blueprint

from app import app

from _helpers import get_credentials

substitution_plans = Blueprint('substitution_plans', __name__,
                               template_folder='templates')


@cross_origin()
@substitution_plans.route('/tomorrow/<schoolclass>/')
def show(page):
    def plan_tomorrow(schoolclass):
#     return plan_for_class(schoolclass, plan_url_tomorrow)

# @cross_origin()
# @app.route('/plan/tomorrow/<schoolclass>/')
# def tomorrow(schoolclass):
#     return plan_for_class(schoolclass, plan_url_tomorrow)
#
#
# @cross_origin()
# @app.route('/plan/today/<schoolclass>/')
# def today(schoolclass):
#     return plan_for_class(schoolclass, plan_url_today)
#
#
# def plan_for_class(schoolclass, plan_url):
#     plan = []
#
#     plan_raw, date = iserv.get_untis_substitution_plan(plan_url, schoolclass)
#     for row in plan_raw:
#         entry_2_text = ""
#         timeWrote = False
#         for entry_2 in row:
#             if timeWrote is False:
#                 time = entry_2
#                 timeWrote = True
#             else:
#                 entry_2_text = " " + entry_2_text + entry_2 + " | "
#
#         # Replace Shortcuts for subjects with their full names
#         replacements = {
#             "MU": "Musik",
#             "VFG": "Verfügung",
#             "PH": "Physik",
#             "EN": "Englisch",
#             "DE": "Deutsch",
#             "SP": "Sport",
#             "KU": "Kunst",
#             "BI": "Bio",
#             "SN": "Spanisch",
#             "FR": "Französisch",
#             "LA": "Latein",
#             "MA": "Mathe",
#             "WN": "Werte u. Normen",
#             "RE": "Religion",
#             "PO": "Politik",
#             "---": "Ausfall"
#         }
#         entry_2_text = entry_2_text.strip()[:-1]
#         for shortcut, longname in replacements.items():
#             entry_2_text = re.sub(r"[→\s]" + shortcut + "[\s]", " " + longname + " ", entry_2_text)
#             entry_2_text = re.sub(r"^" + shortcut + "→", " " + longname + " → ", entry_2_text)
#
#         # create a dict to append to the list of entrys (plan)
#         tempdict = {'time': time, 'content': entry_2_text.strip()}
#         plan.append(tempdict)
#
#     jsontosend = {"plan": plan, "schoolclass": schoolclass, 'date': date}
#     return jsonpify(jsontosend)
