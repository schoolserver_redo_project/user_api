import os

from iservscrapping import Iserv


def get_credentials(request):
    """
    Reads out the login data to authenticate against the server from the request.
    request must be a Flask request object.
    """
    user = request.cookies.get("user")
    password = request.cookies.get("password")
    return user, password


def create_instance(request):
    """
    Returns an instance of the Iserv class.
    """
    (user, password) = get_credentials(request)
    iserv = Iserv(url=os.getenv("base_url"), username=user, password=password)
    return iserv
